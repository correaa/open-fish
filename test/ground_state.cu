#include <inq/inq.hpp>
#include "fishy.hpp"

#include <cassert>
#include <fstream>
#include <functional> // for std::invoke

using std::cout;
using std::endl;

auto const aB = inq::magnitude::operator""_bohr(1);
auto const AA = inq::magnitude::operator""_A(1); // Å
auto const Ha = inq::magnitude::operator""_Ha(1);

template <>
auto fishy::operator/<inq::quantity<inq::magnitude::length, double>>(
    inq::quantity<inq::magnitude::length, double> q,
    fishy::AA_unit_t) -> double {
  return q.in_atomic_units() * (aB / ::AA);
}

int main(int argc, char **argv) {
  inq::input::environment env{argc, argv};

  auto const mol = fishy::crystal::read("POSCAR");
  cout << "density " << exact_mass(mol) / volume(mol.unit_cell()) << '\n';

  auto const [a, b, c] = mol.unit_cell().vectors();

  auto const lat = inq::systems::box::lattice({a.x()/fishy::AA * AA, a.y()/fishy::AA * AA, a.z()/fishy::AA * AA},
                                   {b.x()/fishy::AA * AA, b.y()/fishy::AA * AA, b.z()/fishy::AA * AA},
                                   {c.x()/fishy::AA * AA, c.y()/fishy::AA * AA, c.z()/fishy::AA * AA});

  auto ions = inq::systems::ions{lat};

  std::istringstream iss{1+
R"xyz(
3

O  0.0       -0.2929451 0.0
H  0.75669007 0.2929451 0.0
H -0.75669007 0.2929451 0.0
)xyz"};

  auto const mol2 = fishy::molecule::read(iss, "xyz");

  auto const &as = mol2.atoms();
  for (auto const &atom : as) {
    auto const p = atom.coordinates();
    ions.insert(
      fishy::elements::symbol(atom.atomic_number()),
      {(p.x()/fishy::AA + 2.0) *AA, (p.y()/fishy::AA + 2.0)*AA, (p.z()/fishy::AA + 11.0)*AA}
    );
  }

  std::ofstream ofs("convergence.dat");
  ofs << "# ecut (Ha)   total energy ()" << std::endl;
  for (auto const ecut : {20.0 * Ha, }) {

    auto electrons = inq::systems::electrons(
        env.par(), ions, inq::systems::box{lat}.cutoff_energy(ecut), inq::input::config::extra_states(10),
        inq::input::kpoints::grid({1, 1, 1}, false));

    inq::ground_state::initial_guess(ions, electrons);

    auto const result = inq::ground_state::calculate(
        ions, electrons, inq::input::interaction::pbe(),
        inq::input::scf::steepest_descent() |
            inq::input::scf::energy_tolerance(1e-6 * Ha));

    ofs << ecut << " " << result.energy << std::endl;

    auto const den = inq::observables::density::total(inq::observables::density::calculate(electrons));

    auto const mol_out = std::invoke([cell = ions.cell(), geo = ions.geo(), &den] {
      fishy::molecule ret({cell[0] * aB, cell[1] * aB, cell[2] * aB});

      for (int i = 0; i != geo.atoms().size(); ++i) {
        ret.insert(fishy::atom{geo.atoms()[i].atomic_number(), geo.coordinates()[i] * aB});
      }

      ret.set(fishy::field{
        {den.basis().cell()[0] * aB, den.basis().cell()[1] * aB, den.basis().cell()[2] * aB},
        den.cubic()
      });

      return ret;
    });

    mol_out.output("OUT.POSCAR");
    mol_out.output("OUT.cub");
  }
}
