#ifndef FISH_HPP
#define FISH_HPP

#include <openbabel/atom.h>         // for OpenBabel::OBAtom
#include <openbabel/elements.h> // for OpenBabel::OBElements
#include <openbabel/generic.h>      // for OpenBabel::OBUnitCell
#include <openbabel/griddata.h>     // for OpenBabel::GridData
#include <openbabel/mol.h>          // for OpenBabel::OBMol
#include <openbabel/obconversion.h> // for OpenBabel::OBConversion

#if 0
#include <units/isq/si/length.h>
#include <units/isq/si/mass.h>
#include <units/quantity_io.h>
#else
// #include<boost/units/quantity.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/io.hpp>
#endif

#include <array>
#include <memory>  // for std::unique_ptr
#include <tuple>

#include <filesystem>
// #include <boost/filesystem.hpp>

namespace fishy {

namespace filesystem = std::filesystem;

class AA_unit_t {};

template <class Lenght>
auto operator/(Lenght q, fishy::AA_unit_t)
    -> double = delete; // NVCC needs this

struct cannot : std::runtime_error {
  cannot(std::string const &action_descr, std::string const &reason_descr)
      : std::runtime_error{"cannot " + action_descr + " because " +
                           reason_descr} {}
};

[[noreturn]] void cant(std::string action_description) {
  try {
    throw;
  } catch (std::exception const &e) {
    throw cannot{action_description, e.what()};
  }
  throw cannot{action_description, "unknown reason"};
}

using std::get;

#if 0
  static auto const AA = 1.0e-10 * units::isq::si::references::m;
#else
  static auto const AA = 1.0e-10 * boost::units::si::meter;
#endif

template <class Unit = AA_unit_t> // = typename magnitude_traits<T>::unit>
class vector_3D : OpenBabel::vector3 {
  //  template<class X, class Y, class Z>
 public:
  vector_3D(OpenBabel::vector3 v3) : OpenBabel::vector3{v3} {}
  friend auto vectors(OpenBabel::OBUnitCell const &)
      -> std::array<OpenBabel::vector3, 3>;
#if 0
  static constexpr auto m = units::isq::si::references::m;
#else
  static constexpr auto m = boost::units::si::meter;
#endif
  template<class> friend class cell;

 public:
  template <class T2>
  vector_3D(T2 const &x, T2 const &y, T2 const &z)
      : OpenBabel::vector3(x / Unit{}, y / Unit{}, z / Unit{}) {}  // check ubits

  template<class CartesianVector, std::enable_if_t<not std::is_base_of_v<vector_3D, CartesianVector>, int> =0>
  vector_3D(CartesianVector const& cv) : vector_3D(get<0>(cv), get<1>(cv), get<2>(cv)) {}

  auto x() const {
    //    using namespace units::isq::si::references; // m
    return vector3::x() * (1.0e-10 * m);
  }
  auto y() const {
    //    using namespace units::isq::si::references; // m
    return vector3::y() * (1.0e-10 * m);
  }
  auto z() const {
    //    using namespace units::isq::si::references; // m
    return vector3::z() * (1.0e-10 * m);
  }
  template <class> friend class unit_cell;
  friend class atom;
  friend class field;
};

class atom : OpenBabel::OBAtom {
 public:
  atom(int z) {OpenBabel::OBAtom::SetAtomicNum(z);}
  atom(int z, vector_3D<> v) : atom(z) {set_coordinates(v);}
  atom& set_coordinates(vector_3D<> v) {
    OpenBabel::OBAtom::SetVector(static_cast<OpenBabel::vector3>(v));
    return *this;
  }
  int atomic_number() const { return OpenBabel::OBAtom::GetAtomicNum(); }
  vector_3D<> const& coordinates() const {
    return static_cast<vector_3D<> const&>(OpenBabel::OBAtom::GetVector());
  }
  friend class molecule;
  friend class atom_const_iterator;
};

class atom_const_iterator {
  OpenBabel::OBAtomIterator impl_;
  atom_const_iterator(OpenBabel::OBAtomIterator impl) : impl_{impl} {}
  friend class atoms_type;

public:
  using difference_type = OpenBabel::OBAtomIterator::difference_type;
  using value_type = atom;
  using pointer = atom const*;
  using reference = atom const&;
  using iterator_category = OpenBabel::OBAtomIterator::iterator_category;

  auto operator++() -> atom_const_iterator & {
    ++impl_;
    return *this;
  }
  auto operator--() -> atom_const_iterator & {
    --impl_;
    return *this;
  }
  auto operator+=(difference_type n) -> atom_const_iterator & {
    impl_ += n;
    return *this;
  }
  auto operator-=(difference_type n) -> atom_const_iterator & {
    impl_ += n;
    return *this;
  }

  auto operator* () const -> reference { return static_cast<atom const&>(**impl_); }
  auto operator->() const -> pointer   { return static_cast<atom const*>(*impl_); }

  bool operator==(atom_const_iterator const &o) const {
    return impl_ == o.impl_;
  }
  bool operator!=(atom_const_iterator const &o) const {
    return impl_ != o.impl_;
  }
};

class atoms_type {
  OpenBabel::OBAtomIterator begin_;
  OpenBabel::OBAtomIterator end_;
  unsigned int size_;

  atoms_type &operator=(atoms_type &&) = delete;
  atoms_type(OpenBabel::OBAtomIterator begin, OpenBabel::OBAtomIterator end,
             unsigned int size)
      : begin_{begin}, end_{end}, size_{size} {}
  friend auto atoms(OpenBabel::OBMol const &);

public:
  auto begin() const { return atom_const_iterator{begin_}; };
  auto end() const { return atom_const_iterator{end_}; };
  auto size() const { return size_; }
};

auto atoms(OpenBabel::OBMol const &mol) {
  return atoms_type{
      const_cast<OpenBabel::OBMol &>(mol)
          .BeginAtoms(), // or OpenBabel::OBMolAtomIter(mol)
      const_cast<OpenBabel::OBMol &>(mol).EndAtoms(), // some centinel
      mol.NumAtoms()};
}

namespace conversion {

void read(std::istream &is, OpenBabel::OBMol &mol, std::string_view fmt) try {
  assert(
      fmt !=
      "POSCAR"); // poscar must be read by ReadFile
                 // https://openbabel.org/docs/current/FileFormats/VASP_format.html
  OpenBabel::OBConversion cnv{&is};
  if (not cnv.SetInFormat(fmt.data())) {
    throw std::runtime_error{"cannot find in format `" + std::string{fmt} +
                             "` check that BABEL_LIBDIR points to plugins"};
  }
  if (not cnv.Read(&mol)) {
    throw std::runtime_error{"cannot read format `" + std::string{fmt} +
                             "`, check consistency and that BABEL_DATADIR "
                             "points to data (txt files)"};
  }
} catch (...) {
  cant("fishy::read from stream into molecule as format `" + std::string{fmt});
}

void read_file(OpenBabel::OBMol &mol, std::string const &filename,
               std::string_view fmt) try {
  OpenBabel::OBConversion cnv;
  if (not cnv.SetInFormat(fmt.data())) {
    throw std::runtime_error{"cannot find format `" + std::string{fmt} +
                             "`, check that BABEL_LIBDIR points to plugins"};
  }
  if (not cnv.ReadFile(&mol, filename.c_str())) {
    throw std::runtime_error{
        "OBConversion::ReadFile failed, check consistency and that "
        "BABEL_DATADIR points to data (txt files)"};
  }

  std::clog << "Read " << mol.NumAtoms() << " from file " << filename
            << std::endl;
} catch (std::exception &e) {
  throw cannot{"fishy::read file `" + filename + "` as format `" +
                   std::string{fmt},
               e.what()};
}

//! "fishy::read molecule from path `"+ p.string() +"` as format `"+
//! std::string{fmt} +"`"
void read(OpenBabel::OBMol &mol, filesystem::path const &p,
          std::string_view fmt) try {
  if (not exists(p)) {
    throw std::runtime_error("path `" + p.string() + "` does not exist.");
  }
  return read_file(mol, p.string(), fmt);
} catch (std::exception &e) {
  throw cannot{"fishy::read molecule from path `" + p.string() +
                   "` as format `" + std::string{fmt} + "`",
               e.what()};
}

void read(OpenBabel::OBMol &mol, filesystem::path const &p) try {
  if (p.filename().string() == "POSCAR") {
    return read(mol, p, "POSCAR");
  }
  if (p.extension().string().size() < 2)
    throw std::runtime_error("cannot derive format from extension `" +
                             p.extension().string() + " or stem " +
                             p.stem().string());
  return read(mol, p, p.extension().string().substr(1).c_str());
} catch (std::exception &e) {
  throw cannot{"fishy::read molecule from path `" + p.string() + "`", e.what()};
}

void write_file(OpenBabel::OBMol const &mol, std::string const &filename,
                std::string_view fmt) try {
  OpenBabel::OBConversion cnv;
  if (not cnv.SetOutFormat(fmt.data())) {
    throw std::runtime_error{"cannot find out format `" + std::string{fmt} +
                             "` check that BABEL_LIBDIR points to plugins"};
  }
  if (not cnv.WriteFile(&const_cast<OpenBabel::OBMol &>(mol),
                        filename.c_str())) {
    throw std::runtime_error{
        "cannot write file `" + filename + "` with format format `" +
        std::string{fmt} +
        "`, check consistency and that env var BABEL_DATADIR "
        "points to data (txt files). Check if the molecule is compatible with "
        "the format (e.g. lacks fields needed by the format)"};
  }
} catch (...) {
  cant("fishy::write_file molecule in filename `" + filename + "`");
}

void write(OpenBabel::OBMol const &mol, filesystem::path const &p,
           std::string_view fmt) {
  return write_file(mol, p.string(), fmt);
}
void write(OpenBabel::OBMol const &mol, filesystem::path const &p) {
  return write(mol, p, p.extension().string().substr(1).c_str());
}

} // end namespace conversion

void fill(OpenBabel::OBUnitCell const &uc, OpenBabel::OBMol &mol) {
  const_cast<OpenBabel::OBUnitCell &>(uc).FillUnitCell(&mol);
}

bool has_unit_cell(OpenBabel::OBMol const &mol) {
  return const_cast<OpenBabel::OBMol &>(mol).HasData(OpenBabel::OBGenericDataType::UnitCell);
}

auto const &get_unit_cell(OpenBabel::OBMol const &mol) {
  assert(has_unit_cell(mol)); // loaded format might not have
                                                // cell
  return static_cast<OpenBabel::OBUnitCell &>(
      *const_cast<OpenBabel::OBMol &>(mol).GetData(
          OpenBabel::OBGenericDataType::UnitCell));
}

void fill_unit_cell(OpenBabel::OBMol &mol) {
  assert(mol.HasData(
      OpenBabel::OBGenericDataType::UnitCell)); // loaded format might not have
                                                // cell
  fill(static_cast<OpenBabel::OBUnitCell &>(
           *mol.GetData(OpenBabel::OBGenericDataType::UnitCell)),
       mol);
}

auto volume(OpenBabel::OBUnitCell const &uc) {
#if 0
  using namespace units::isq::si::references; // m
#else
  auto const m = boost::units::si::meter;
#endif
  return uc.GetCellVolume() * (1.0e-10 * m) * (1.0e-10 * m) * (1.0e-10 * m);
}

auto exact_mass(OpenBabel::OBMol const &mol) {
#if 0
  using namespace units::isq::si::references; // kg
#else
  constexpr auto kg = boost::units::si::kilogram;
#endif
  return const_cast<OpenBabel::OBMol &>(mol).GetExactMass() *
         (1.66054e-27 * kg);
}

template <class T = void> struct magnitude_traits {
  static auto unit(T t = T{}) -> T {
    return T::from_value(1.0);
    ;
  }
};

template <> struct magnitude_traits<void> {
  template <class T> static auto unit(T const &t = T{}) {
    return magnitude_traits<T>::unit(t);
  }
};

template <> struct magnitude_traits<double> {
  static auto unit(double d = 1.0) -> double { return 1.0; }
};

auto vectors(OpenBabel::OBUnitCell const &)
    -> std::array<OpenBabel::vector3, 3>;

// template<class T = double> vector_3D(T, T, T) -> vector_3D<T>;

// template<class Unit, class C>
// auto make_vector_3D(C const& x, C const& y, C const& z) {
//   return vector_3D<Unit>(x, y, z);
// }

// class molecule;

template <class V = fishy::vector_3D<>>
class cell : OpenBabel::OBUnitCell {
  cell(OpenBabel::OBUnitCell const& other) : OpenBabel::OBUnitCell{other} {}
  friend class crystal;

 public:
  template <class V3D>
  cell(V3D const &v1, V3D const &v2, V3D const &v3) : cell(V{v1}, V{v2}, V{v3}) {}
  cell(V const& v1, V const& v2, V const& v3) {OBUnitCell::SetData(v1, v2, v3);}
  template <class VV>
  friend OpenBabel::OBMol &set_cell(OpenBabel::OBMol &, cell<VV> const &);
  friend class molecule;
  auto operator[](std::size_t i) const {
    return static_cast<fishy::vector_3D<>>(OBUnitCell::GetCellMatrix().GetRow(i));
  //  return OBUnitCell::GetCellVectors()[i];
  }
  auto volume() const {return fishy::volume(*this);}

  auto vectors() const {
    return std::array<fishy::vector_3D<>, 3> {
      static_cast<fishy::vector_3D<>>(OBUnitCell::GetCellMatrix().GetRow(0)),
      static_cast<fishy::vector_3D<>>(OBUnitCell::GetCellMatrix().GetRow(1)),
      static_cast<fishy::vector_3D<>>(OBUnitCell::GetCellMatrix().GetRow(2))
    };
  }

  template<std::size_t Index>
  friend fishy::vector_3D<> get(cell<> const& self);
  template<std::size_t Index>
  friend fishy::vector_3D<> get(cell<> const&& self);
  template<std::size_t Index>
  friend fishy::vector_3D<> get(cell<> & self);
  template<std::size_t Index>
  friend fishy::vector_3D<> get(cell<> && self);

};

auto volume(cell<> const& c) {return c.volume();}

template<std::size_t Index>
fishy::vector_3D<> get(cell<> const& self) { static_assert(Index >=0 and Index < 3);
  return self[Index];
}

template<std::size_t Index>
fishy::vector_3D<> get(cell<> const&& self) { static_assert(Index >=0 and Index < 3);
  return self[Index];
}

template<std::size_t Index>
fishy::vector_3D<> get(cell<>& self) { static_assert(Index >=0 and Index < 3);
  return self[Index];
}

template<std::size_t Index>
fishy::vector_3D<> get(cell<>&& self) { static_assert(Index >=0 and Index < 3);
  return std::move(self)[Index];
}

}

namespace std {
	template<>
	struct tuple_size<fishy::cell<> > : integral_constant<size_t, 3> {};

	template<std::size_t N>
	struct tuple_element<N, fishy::cell<> > {using type = fishy::vector_3D<>;};

  using fishy::get;
}

namespace fishy {

using std::get;

struct field : private std::unique_ptr<OpenBabel::OBGridData> {
  field(fishy::cell<> const& cell, std::array<long, 3> sizes) :
    std::unique_ptr<OpenBabel::OBGridData>{std::make_unique<OpenBabel::OBGridData>()}
  {
    (*this)->SetNumberOfPoints(std::get<0>(sizes), std::get<1>(sizes), std::get<2>(sizes));
    (*this)->SetLimits(
        OpenBabel::vector3{0.0, 0.0, 0.0},
        static_cast<OpenBabel::vector3 const&>(fishy::get<0>(cell))/ std::get<0>(sizes),
        static_cast<OpenBabel::vector3 const&>(fishy::get<1>(cell))/ std::get<1>(sizes),
        static_cast<OpenBabel::vector3 const&>(fishy::get<2>(cell))/ std::get<2>(sizes)
    );
    (*this)->SetUnit(OpenBabel::OBGridData::ANGSTROM);

    // auto gd = std::make_unique<OpenBabel::OBGridData>();
    // gd->SetNumberOfPoints(is.size(), js.size(), ks.size());

    // gd->SetLimits({0.0, 0.0, 0.0},
    //               {den.basis().cell()[0][0] / is.size() * aB / AA,
    //                den.basis().cell()[0][1] / is.size() * aB / AA,
    //                den.basis().cell()[0][2] / is.size() * aB / AA},
    //               {den.basis().cell()[1][0] / js.size() * aB / AA,
    //                den.basis().cell()[1][1] / js.size() * aB / AA,
    //                den.basis().cell()[1][2] / js.size() * aB / AA},
    //               {den.basis().cell()[2][0] / ks.size() * aB / AA,
    //                den.basis().cell()[2][1] / ks.size() * aB / AA,
    //                den.basis().cell()[2][2] / ks.size() * aB / AA});
    // // gd->SetLimits(origin, axes[0], axes[1], axes[2]);
    // gd->SetUnit(OpenBabel::OBGridData::ANGSTROM);
    // // gd->SetOrigin(fileformatInput); // i.e., is this data from a file or
    // // determined by Open Babel

    // double count = 0.0;
    // for (auto const i : is) {
    //   for (auto const j : js) {
    //     for (auto const k : ks) {
    //       gd->SetValue(i, j, k, den.hypercubic().rotated()[0][i][j][k]);
    //       count += den.hypercubic().rotated()[0][i][j][k];
    //     }
    //   }
    // }
    // std::cout << "electron count = " << count << std::endl;

    // //   mol_out.SetData(gd.release());

  }
  field(fishy::cell<fishy::vector_3D<fishy::AA_unit_t>> const& cell) : field(cell, {1, 1, 1}) {
    (*this)->SetValue(0, 0, 0, 0.0);
  }
  template<class Array3D, class = decltype(std::declval<Array3D const&>()[0][0][0])>
  field(fishy::cell<fishy::vector_3D<fishy::AA_unit_t>> const& cell, Array3D const& A)
  : field(cell, std::apply([](auto... e){return std::array<long, 3>{e...};}, A.sizes()))
  {
    set_cubic_data(A);
  }

  auto cubic_data_sizes() const {
    std::array<int, 3> ret;
    (*this)->GetNumberOfPoints(std::get<0>(ret), std::get<1>(ret), std::get<2>(ret));
    return ret;
  }

  template<class Array3D>
  void set_cubic_data(Array3D const& A) {
    auto const [in, jn, kn] = A.sizes();
    [[maybe_unused]] auto const [m, n, l] = cubic_data_sizes();
    assert(in == m and jn == n and kn == l);
    for(auto i = 0; i != in; ++i) {
      for(auto j = 0; j != jn; ++j) {
        for (auto k = 0; k != kn; ++k) {
          (*this)->SetValue(i, j, k, A[i][j][k]);
        }
      }
    }
  }

  friend class molecule;
};

class molecule : OpenBabel::OBMol {
  molecule() = default;

 public:
  molecule(fishy::cell<fishy::vector_3D<fishy::AA_unit_t>> const & cell) {
    OpenBabel::OBMol::SetData(new auto{static_cast<OpenBabel::OBUnitCell const &>(cell)});
  }
  auto insert(OpenBabel::OBAtom const& at) -> OpenBabel::OBAtom* {
    [[maybe_unused]] bool const check = OpenBabel::OBMol::InsertAtom(const_cast<OpenBabel::OBAtom&>(at));
    assert(check);
    OpenBabel::OBAtom* ret = OpenBabel::OBMol::GetAtom(OpenBabel::OBMol::NumAtoms());  // indexing is one-based
    assert(ret);
    return ret;
  }
  auto insert(atom const& at) {
    return insert(static_cast<OpenBabel::OBAtom const&>(at));
  }
  auto set(field&& fi) {
    OpenBabel::OBMol::SetData(static_cast<std::unique_ptr<OpenBabel::OBGridData>&&>(fi).release());
  }

  friend decltype(auto) ucell(molecule const& self) {
    return get_unit_cell(self);
  }
  friend auto exact_mass(molecule const& self) {
    return exact_mass(static_cast<OpenBabel::OBMol const&>(self));
  }

  static auto read(filesystem::path const &p) {
    molecule ret;
    conversion::read(static_cast<OpenBabel::OBMol&>(ret), p);
    return ret;
  }

  static auto read(std::istream& is, std::string fmt) {
    molecule ret;
    conversion::read(is, static_cast<OpenBabel::OBMol&>(ret), fmt);
    return ret;
  }

  void output(filesystem::path const& p) const {
    conversion::write(static_cast<OpenBabel::OBMol const&>(*this), p);
  }
  auto atoms() const {
    return fishy::atoms(static_cast<OpenBabel::OBMol const&>(*this));
  }

  friend auto atoms(molecule const& self) {
    return fishy::atoms(static_cast<OpenBabel::OBMol const&>(self));
  }
  bool has_cell() const {
    return fishy::has_unit_cell(static_cast<OpenBabel::OBMol const&>(*this));
  }
  friend class crystal;
};

struct crystal : molecule {
 private:
  crystal(molecule const& other) : molecule{other} {}

 public:
  static auto read(filesystem::path const &p) -> crystal {
    molecule ret;
    try {
      conversion::read(static_cast<OpenBabel::OBMol&>(ret), p);
      if(not ret.has_cell()) {
        throw "has not unit cell";
      }
    } catch(...) {
      cant("fishy::crystal::read from "+ p.string() +" because file doesn't have a cell. Try reading it as a molecule and inspect it.");
    }
    return ret;
  }  
  auto unit_cell() const -> cell<> {return fishy::get_unit_cell(*this);}
  decltype(auto) basis_atoms() const {return fishy::atoms(*this);}
};

template <class V>
OpenBabel::OBMol &set_cell(OpenBabel::OBMol &mol, cell<V> const &c) {
  mol.SetData(new auto{static_cast<OpenBabel::OBUnitCell const &>(c)});
  return mol;
}

auto vectors(OpenBabel::OBUnitCell const &c)
    -> std::array<OpenBabel::vector3, 3> {
  auto vecs = c.GetCellVectors();
  return std::array<OpenBabel::vector3, 3>{vecs[0], vecs[1], vecs[2]};
}

struct elements {
  static auto symbol(int z_number) {return OpenBabel::OBElements::GetSymbol(z_number);}
};

struct grid_data : private std::unique_ptr<OpenBabel::OBGridData> {
  template <class Field, class Scale>
  explicit grid_data(Field const &den, Scale const &s)
      : std::unique_ptr<OpenBabel::OBGridData>{
            std::make_unique<OpenBabel::OBGridData>()} {
    auto [is, js, ks, components] = den.hypercubic().extensions();
    if (components != 1)
      throw;

    (*this)->SetNumberOfPoints(is.size(), js.size(), ks.size());
    (*this)->SetLimits(OpenBabel::vector3{0.0, 0.0, 0.0},
                       OpenBabel::vector3{den.basis().cell()[0][0] / is.size() * s,
                                          den.basis().cell()[0][1] / is.size() * s,
                                          den.basis().cell()[0][2] / is.size() * s},
                       OpenBabel::vector3{den.basis().cell()[1][0] / js.size() * s,
                                          den.basis().cell()[1][1] / js.size() * s,
                                          den.basis().cell()[1][2] / js.size() * s},
                       OpenBabel::vector3{den.basis().cell()[2][0] / ks.size() * s,
                                          den.basis().cell()[2][1] / ks.size() * s,
                                          den.basis().cell()[2][2] / ks.size() * s});
    (*this)->SetUnit(OpenBabel::OBGridData::BOHR);

    for (auto const i : is) {
      for (auto const j : js) {
        for (auto const k : ks) {
          (*this)->SetValue(i, j, k, den.hypercubic()[i][j][k][0]);
        }
      }
    }
  }

  friend OpenBabel::OBMol &set_grid_data(OpenBabel::OBMol &mol,
                                         grid_data &&den) {
    mol.SetData(den.release());
    return mol;
  }
};
} // end namespace fishy

#endif